package demo.syncproxyex.test;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by leon on 16/8/22.
 */
public class BTReceiver extends BroadcastReceiver
{
	@Override
	public void onReceive(Context context, Intent intent)
	{
		final BluetoothDevice bluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
		if (intent.getAction() != null)
		{
			if (intent.getAction().compareTo(BluetoothDevice.ACTION_ACL_CONNECTED) == 0)
			{
				Intent i = new Intent(context, SyncProxyService.class);
				context.startService(i);
			}
			else if (intent.getAction().equals(BluetoothAdapter.ACTION_STATE_CHANGED))
			{
				if ((intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) == (BluetoothAdapter.STATE_TURNING_OFF)))
				{
					Intent i = new Intent(context, SyncProxyService.class);
					context.stopService(i);
				}
			}

		}
	}
}
